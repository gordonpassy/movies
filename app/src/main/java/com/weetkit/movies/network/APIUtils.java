package com.weetkit.movies.network;

/**
 * Created by gordonpassy on 21/08/2017.
 */

public class APIUtils {
    private static final String BASE_URL="https://api.themoviedb.org/3/";
    public static final String IMAGE_BASE_URL="http://image.tmdb.org/t/p/w342/";//w185
    public static final String API_KEY="623d8d38a876cc05c3b87575c7878c05";

    public static APIService getApiService(){
        return RetrofitClient.getClient(BASE_URL).create(APIService.class);
    }
}
